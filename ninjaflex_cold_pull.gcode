M109 S230       ; Set extruder to 230C
M907 E400       ; Reduce extruder torque
G92 E0          ; Set extruder location to 0
M104 S0         ; set temp to 0C
G1 E60 F20      ; extruder 60mm filament slowly
M907 E675       ; set extruder torque to normal
M302            ; allow cold extrude
M109 R61        ; wait for hotend to get cold
M400            ; clear buffer
G1 E0 F60       ; retract filament to build up tension
M109 S100       ; heat to 100 to get filament to pop out of noozle
M104 S0         ; turn off hotend
